terraform {
  required_version = ">= 0.12"
}
provider "aws" {
  region = var.region
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = var.bucket_name

  versioning  {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }
}
