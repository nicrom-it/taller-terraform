variable "bucket_name" {
  description = "The name of the S3 bucket. Must be globally unique."
  default = "taller2019"
}
variable "region" {
  type        = string
  default     = "sa-east-1"
  description = "zona S3 on save terraform.tfstate"
}