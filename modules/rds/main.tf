resource "aws_db_subnet_group" "subn-groups" {
  //subnet_ids = ["${module.vpc.sub_pri1}", "${module.vpc.sub_pri2}"]
  subnet_ids = [var.vpc-sub_pri1,var.vpc-sub_pri2]
}

resource "aws_db_instance" "mysql" {
  instance_class         = var.instance_class
  identifier             = var.identifier
  name                   = var.name
  username               = var.username
  password               = var.password
  engine                 = var.engine
  engine_version         = var.engine_version
  allocated_storage      = var.allocated_storage
  storage_type           = var.storage_type
  multi_az               = var.multi_az
  db_subnet_group_name   = aws_db_subnet_group.subn-groups.name
  vpc_security_group_ids = var.vpc_security_group_ids
  publicly_accessible    = var.publicly_accessible
  skip_final_snapshot    = var.skip_final_snapshot
}
