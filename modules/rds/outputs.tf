output "rds_adress" {
  description = "The cluster endpoint"
  value       = aws_db_instance.mysql.address
}
