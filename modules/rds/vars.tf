variable "vpc-sub_pri1" {
  type = string
}
variable "vpc-sub_pri2" {
  type = string
}
variable "name" {
  type = string
}
variable "username" {
  type = string
}
variable "password" {
  type = string
}
variable "vpc_security_group_ids" {
  type = list
}
variable "instance_class" {
  type = string
}
variable "identifier" {
  type = string
}
variable "engine" {
  type = string
}
variable "engine_version" {
  type = string
}
variable "allocated_storage" {
  type = string
}
variable "storage_type" {
  type = string
}
variable "multi_az" {
  default = false
}
variable "publicly_accessible" {
  default = true
}
variable "skip_final_snapshot" {
  default = true
}
