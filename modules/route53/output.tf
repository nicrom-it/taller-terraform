output "parent_zone_id" {
  value       = aws_route53_zone.default.*.zone_id
  description = "ID of the hosted zone to contain this record"
}

output "parent_zone_name" {
  value       = aws_route53_zone.default.name_servers
  description = "Name of the hosted zone to contain this record"
}