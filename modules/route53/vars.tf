variable "zone_name" {
  description = "Name of the hosted zone to contain this record (or specify `parent_zone_id`)"
}

