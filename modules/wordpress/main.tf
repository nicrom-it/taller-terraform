data "template_file" "init" {
 template = file("../../modules/wordpress/templates/userdata.tpl")
   vars = {
    name= var.name
    rds_username= var.rds_username
    rds_passwd= var.rds_passwd
    endpoint= var.endpoint
  }
}

resource "aws_instance" "wordpress" {
  ami = var.ami_id
  tags = {
    Name = "${var.project}-${var.environment}-ec2-wordpress"
  }
  instance_type = var.instance_type
  user_data = base64encode(data.template_file.init.rendered)
  key_name = var.key_name
  vpc_security_group_ids = [var.vpc_security_group_ids]
  subnet_id = var.subnetpublic1
  lifecycle {
    create_before_destroy = true
  }
}