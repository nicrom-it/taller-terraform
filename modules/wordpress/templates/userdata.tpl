#!/bin/bash

# Instalando Docker y Docker-Compose

curl -sSL https://get.docker.com/ | sh

sleep 5
sudo usermod -a -G docker ubuntu
sudo systemctl start docker
sudo systemctl enable docker

sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
    && sudo chmod +x /usr/local/bin/docker-compose \

# Instalando LazyDocker

curl https://raw.githubusercontent.com/jesseduffield/lazydocker/master/scripts/install_update_linux.sh | bash

# Agregando docker-compose.yaml

mkdir -p /home/ubuntu/wordpress && cd "$_"


cat <<'EOF' >> docker-compose.yaml
version: '2'

services:
#  nginx-proxy:
#    image: jwilder/nginx-proxy
#    container_name: nginx-proxy
#    restart: always
#    ports:
#      - "80:80"
#      - "443:443"
#    volumes:
#    - /var/run/docker.sock:/tmp/docker.sock:ro
#    - /docker/certs:/etc/nginx/certs:ro
#    - /etc/nginx/vhost.d
#    - /usr/share/nginx/html
#    labels:
#    - com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy
#
#  letsencrypt:
#    image: jrcs/letsencrypt-nginx-proxy-companion
#    container_name: nginx-proxy-letsencrypt
#    restart: always
#    volumes:
#      - /docker/certs:/etc/nginx/certs:rw
#      - /var/run/docker.sock:/var/run/docker.sock:ro
#    volumes_from:
#      - nginx-proxy:rw

  wordpress:
    image: wordpress:php7.2-apache
    restart: always
    ports:
      - "80:80"
    environment:
      VIRTUAL_HOST: nicromit.com, www.nicromit.com
      LETSENCRYPT_HOST: nicromit.com
      LETSENCRYPT_EMAIL: contacto@nicromit.com
      WORDPRESS_DB_HOST: ${endpoint}
      WORDPRESS_DB_USER: ${rds_username}
      WORDPRESS_DB_PASSWORD: ${rds_passwd}
      WORDPRESS_DB_NAME: ${name}

EOF

docker-compose up -d