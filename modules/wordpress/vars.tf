variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "ami_id" {
  type = string
  default = "ami-02a3447be1ec3a38f"
}
variable "key_name" {
  type = string
}
variable "environment" {
  type    = string
  default = "stage"
}
variable "project" {
  type    = string
}

variable "vpc_security_group_ids" {
  type = string
}
variable "subnetpublic1" {
}
variable "name" {}
variable "rds_username" {}
variable "rds_passwd" {}
variable "endpoint" {}
