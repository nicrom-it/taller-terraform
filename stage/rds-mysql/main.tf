terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region = var.region
}
data "aws_subnet" "subnetpri1" {
  filter {
    name   = "tag:Name"
    values = ["${var.project}-${var.environment}-pri1"]
  }
}
data "aws_subnet" "subnetpri2" {
  filter {
    name   = "tag:Name"
    values = ["${var.project}-${var.environment}-pri2"]
  }
}


data "aws_security_group" "selector" {
  filter {
    name   = "tag:Name"
    values = ["${var.project}-${var.environment}-sg-rds"]
  }
}
module "rds_mysql" {
  source                 = "../../modules/rds"
  name                   = var.name
  instance_class         = var.instance_class
  identifier             = "${var.project}-${var.environment}-rds"
  engine                 = var.engine
  engine_version         = var.engine_version
  allocated_storage      = var.allocated_storage
  storage_type           = var.storage_type
  multi_az               = var.multi_az
  password               = var.rds_passwd
  username               = var.rds_username
  vpc-sub_pri1           = data.aws_subnet.subnetpri1.id
  vpc-sub_pri2           = data.aws_subnet.subnetpri2.id
  vpc_security_group_ids = [data.aws_security_group.selector.id]
}
