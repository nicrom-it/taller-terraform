output "rds_adress" {
  description = "The cluster endpoint"
  value       = module.rds_mysql.rds_adress
}
