terraform {
  backend "s3" {
    encrypt = true
    bucket  = "taller2019"
    region  = "sa-east-1"
    key     = "terraform/wordpress/stage/rds/terraform.tfstate"
  }
}
