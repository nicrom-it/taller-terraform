variable "region" {
  description = "Amazon region"
  default     = "sa-east-1"
}
variable "project" {
  default = "wordpress"
}
variable "environment" {
  default = "stage"
}
variable "instance_class" {
  type    = string
  default = "db.t2.micro"
}
variable "engine" {
  type    = string
  default = "mysql"
}
variable "engine_version" {
  type    = string
  default = "8.0.16"
}
variable "allocated_storage" {
  type    = string
  default = 10
}
variable "storage_type" {
  type    = string
  default = "gp2"
}
variable "multi_az" {
  default = false
}
variable "publicly_accessible" {
  default = false
}
variable "skip_final_snapshot" {
  default = true
}

variable "name" {
  type = string
}
variable "rds_username" {
  type = string
}
variable "rds_passwd" {
  type = string
}
