data "aws_eip" "ip-public" {
  filter {
    name = "tag:Name"
    values = ["${var.project}-${var.environment}-ip-elastic"]
  }
}

resource "aws_route53_record" "emediapp" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = "nicrom.cloud"
  type    = "A"
  ttl     = "300"
  records = [data.aws_eip.ip-public.public_ip]
}