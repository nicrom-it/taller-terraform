terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region = var.region
}

module "route53" {
  source    = "../../modules/route53"
  zone_name = var.zone_name
}

data "aws_route53_zone" "selected" {
  name         = "nicrom.cloud."
}