
output "parent_zone_id" {
  value       = module.route53.parent_zone_id
  description = "ID of the hosted zone to contain this record"
}

output "parent_zone_name" {
  value       = module.route53.parent_zone_name
  description = "Name of the hosted zone to contain this record"
}
