variable "region" {
  description = "Amazon region"
  default = "sa-east-1"
}
variable "project" {
  description = "Project Name"
  default = "wordpress"
}
variable "environment" {
  description = "Workdirectory"
  default = "stage"
}
variable "zone_name" {
  description = "Name of the hosted zone to contain this record (or specify `parent_zone_id`)"
  default     = "nicrom.cloud"
}