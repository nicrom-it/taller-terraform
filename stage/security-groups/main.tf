terraform {
  required_version = ">= 0.12"
}
provider "aws" {
  region = var.region
}

data "aws_vpc" "selected" {
  filter {
    name = "tag:Name"
    values = ["${var.project}-${var.environment}-vpc"]
  }
  
}

//data "aws_security_group" "selector" {
//  filter {
//    name   = "tag:Name"
//    values = ["Wordpress-Nicromit"]
//  }
//}