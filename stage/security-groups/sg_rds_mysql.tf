resource "aws_security_group" "wordpress-rds-sg" {
  name   = "${var.project}-${var.environment}-rds-sg"
  vpc_id = data.aws_vpc.selected.id
  ingress {
    from_port       = 3306
    protocol        = "tcp"
    to_port         = 3306
    security_groups = [aws_security_group.wordpress-sg.id]
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.project}-${var.environment}-sg-rds"
  }
}
