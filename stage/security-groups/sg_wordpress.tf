resource "aws_security_group" "wordpress-sg" {
  name   = "${var.project}-${var.environment}-sg"
  vpc_id = data.aws_vpc.selected.id
  ingress {
    from_port       = 80
    protocol        = "tcp"
    to_port         = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port       = 443
    protocol        = "tcp"
    to_port         = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.project}-${var.environment}-sg"
  }
}