variable "region" {
  description = "Amazon region"
  default = "sa-east-1"
}
variable "project" {
  description = "Project Name"
  default = "wordpress"
}
variable "environment" {
  description = "Workdirectory"
  default = "stage"
}
