terraform {
  required_version = ">= 0.12"
}
provider "aws" {
  region = var.region
}

module "vpc" {
  source = "../../modules/vpc"
  environment = var.environment
  project = var.project
}