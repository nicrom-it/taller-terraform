# VPC
output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}
# Subnets
output "private_subnets" {
  description = "List of IDs of private subnets"
  value       =  [module.vpc.sub_pri1, module.vpc.sub_pri2]
}
output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = [module.vpc.sub_pub1, module.vpc.sub_pub2]
}
output "az" {
  value = [module.vpc.az[0],module.vpc.az[1]]
}
