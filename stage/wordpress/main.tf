terraform {
  required_version = ">= 0.12"
}
provider "aws" {
  region = var.region
}
data "aws_subnet" "subnetpub1" {
  filter {
    name   = "tag:Name"
    values = ["${var.project}-${var.environment}-pub1"]
  }
}
data "aws_security_group" "selector" {
  filter {
    name   = "tag:Name"
    values = ["${var.project}-${var.environment}-sg"]
  }
}
data "aws_db_instance" "database" {
  db_instance_identifier = "${var.project}-${var.environment}-rds"
}
module "wordpress" {
  source = "../../modules/wordpress"
  ami_id = var.ami_id
  key_name = "deployer-key"
  project = var.project
  environment = var.environment
  subnetpublic1 = data.aws_subnet.subnetpub1.id
  vpc_security_group_ids = data.aws_security_group.selector.id
  instance_type = var.instance_type
  name= var.name
  rds_username= var.rds_username
  rds_passwd= var.rds_passwd
  endpoint= data.aws_db_instance.database.address

}
resource "aws_eip" "eip" {
  vpc = true
  tags = {
    Name = "${var.project}-${var.environment}-ip-elastic"
  }
}
resource "aws_eip_association" "eip_assoc" {
  instance_id   = module.wordpress.instance_id
  allocation_id = aws_eip.eip.id
}
