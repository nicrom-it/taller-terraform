output "public_ip" {
  description = "The ID of the VPC"
  value       = aws_eip.eip.public_ip
}

output "instance_id" {
  value = module.wordpress.instance_id
}