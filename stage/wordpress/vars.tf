variable "region" {
  description = "Amazon region"
  default = "sa-east-1"
}
variable "project" {
  description = "Project Name"
  default = "wordpress"
}
variable "environment" {
  description = "Workdirectory"
  default = "stage"
}

variable "ami_id" {
  default = "ami-02a3447be1ec3a38f"
}
variable "key_name" {
  default = "taller2019"
}
variable "instance_type" {
  default = "t2.micro"
}


variable "name" {
  type = string
}
variable "rds_username" {
  type = string
}
variable "rds_passwd" {
  type = string
}
